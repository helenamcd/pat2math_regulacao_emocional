<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	

<form:form action="." modelAttribute="exercise" accept-charset="utf-8" class="box block left">
	<form:errors path="*">
		<div class="error-global">
			<spring:message code="error.global" />
		</div>
	</form:errors>
	
	<label>Equação</label>
	<p>
		<form:input path="equation" />
	</p>
	<label>Nível da Questão</label>
	<p>
		<form:select path="nivelDificuldade">
			<c:forEach var = "i" begin = "1" end = "10">
	        	<form:option value="${i}"/>
	      	</c:forEach>
		</form:select>
	</p>
	
	<label>Hash da Emoção</label>
	<div >
		<p>
			<label>Ação 1(Texto Qualquer):</label>
			<form:input path="acao1"/>
		</p>
		<p>
			<label>Ação 2(Figura Qualquer):</label>
			<form:input path="acao2" type="file"/>
		</p>
		<p>
			<label>Ação 3(Vídeo Qualquer):</label>
			<form:input path="url"/>
		</p>
	</div>
	
	<p>
		<form:errors path="equation">
			<form:errors path="equation" htmlEscape="false" class="error" />
			<form:errors path="nivelDificuldade" htmlEscape="false" class="error" />
			<form:errors path="acao1" htmlEscape="false" class="error" />
			<form:errors path="acao2" htmlEscape="false" class="error" />
			<form:errors path="url" htmlEscape="false" class="error" />
		</form:errors>
	</p>
	
	<form:hidden path="description" />
	<form:hidden path="name" />
	<br>
	<input type="submit" class="btn btn-large" value="Criar Exercício" />
</form:form>