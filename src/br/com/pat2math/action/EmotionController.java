package br.com.pat2math.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import baseDominio.DAO.ExercicioDAO;
import br.com.pat2math.dao.ContentDao;
import br.com.pat2math.dao.ExerciseDao;
import br.com.pat2math.domainBase.Emotion;
import br.com.pat2math.domainBase.Exercise;
import br.com.pat2math.repository.ContentRepository;
import br.com.pat2math.service.ContentService;
import br.com.pat2math.service.EmotionService;
import br.com.pat2math.service.GroupService;
import br.com.pat2math.studentModel.Group;
import br.com.pat2math.studentModel.Teacher;

@Controller
@Transactional
@RequestMapping("/emotion")
public class EmotionController {
	
	@Autowired private EmotionService emotionservice;
	@Autowired private ContentService contentservice;
	@Autowired private ExerciseDao exercise;
	
	@RequestMapping(value="createemotion/{idexercise}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String save(@PathVariable Long idexercise, @ModelAttribute("emotion") @Valid Emotion emotion, 
						BindingResult result, HttpSession session) {
		Emotion newEmotion = emotionservice.createEmotion(emotion, contentservice.getExercisebyId(idexercise), result);
		if(result.hasErrors())
			return "group.new";
		return "" + newEmotion.getId();
	}
	
	@RequestMapping(value="incrementError/{idemotion}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String incrementError(@PathVariable Long idemotion, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		String ret = emotionservice.incrementError(idemotion);
		
		return "" + ret;
	}
	
	@RequestMapping(value="incrementHints/{idemotion}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String incrementHints(@PathVariable Long idemotion, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		String ret = emotionservice.incrementHints(idemotion);
		
		return "" + ret;
	}
	
	@RequestMapping(value="incrementAbandon/{idemotion}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String incrementAbandon(@PathVariable Long idemotion, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		String ret = emotionservice.incrementAbandon(idemotion);
		
		return "" + ret;
	}
	
	@RequestMapping(value="setpreviousAbandon/{idemotion}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String setpreviousAbandon(@PathVariable Long idemotion, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		String ret = emotionservice.setpreviousAbandon(idemotion);
		
		return "" + ret;
	}
	
	@RequestMapping(value="calculaIntervencao/{idemotion}", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String calculaIntervencao(@PathVariable Long idemotion, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		String ret = emotionservice.calculaIntervencao(idemotion);
		
		return "" + ret;
	}
	
	@RequestMapping(value="/getContentWithLevel", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String getContentWithLevel(@RequestParam("idex") Long idex, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		Exercise atual = contentservice.getExercisebyId(idex);
		
		Exercise e = exercise.getByLevel(atual.getnivelDificuldade(), idex);
		
		if(e == null){
			return "-1";
		}
		
		return "" + e.getId();
	}

	@RequestMapping(value="getAcao1/{id}", method = RequestMethod.GET)
	public @ResponseBody String getAcao1(@PathVariable Long id, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		Exercise e = contentservice.getExercisebyId(id);
		return e.getAcao1();
	}
	
	@RequestMapping(value="getAcao3/{id}", method = RequestMethod.GET)
	public @ResponseBody String getAcao3(@PathVariable Long id, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		Exercise e = contentservice.getExercisebyId(id);
		return e.geturl();
	}
	
	@RequestMapping(value="/setExerciseId", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String setExerciseId(@RequestParam("idemotion") Long idemotion,@RequestParam("idex") Long idex, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		String ret = emotionservice.setExerciseId(idemotion, idex);
		
		return "" + ret;
	}
	
	@RequestMapping(value="/setvalemotion", method = RequestMethod.GET, produces="text/plain; charset=UTF-8")
	public @ResponseBody String setvalemotion(@RequestParam("idemotion") Long idemotion,@RequestParam("val") Long val, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		
		String ret = emotionservice.setValEmotion(idemotion, val);
		
		return "" + ret;
	}
	

}
