package br.com.pat2math.dao;

import org.springframework.stereotype.Repository;

import br.com.pat2math.domainBase.Emotion;
import br.com.pat2math.domainBase.Exercise;
import br.com.pat2math.repository.EmotionRepository;


@Repository
public class EmotionDao extends GenericDao<Emotion> implements EmotionRepository{

	@Override
	public Emotion getRaw(Long id) {
		String queryStr =
		    "select NEW br.com.pat2math.domainBase.Exercise(e.id, e.name, e.equation) " +
		    "from Exercise e where e.id=:id";
		
		return em.createQuery(queryStr, Emotion.class)
					.setParameter("id", id)
					.getSingleResult();
	}

}
