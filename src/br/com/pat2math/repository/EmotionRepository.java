package br.com.pat2math.repository;

import br.com.pat2math.domainBase.Emotion;


public interface EmotionRepository extends Repository<Emotion>{
	Emotion getRaw(Long id);
}
